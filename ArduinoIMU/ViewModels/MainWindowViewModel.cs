﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArduinoIMU.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public MainWindowViewModel()
        {
        }

        public void Start()
        {
            f.StartNew(() =>
            {
                var portNames = SerialPort.GetPortNames();
                if (portNames.Count() > 0)
                {
                    SerialPort port1 = new SerialPort(portNames[0], 115200, Parity.None, 8, StopBits.One);
                    port1.NewLine = "\r\n";
                    port1.Open();
                    while (true)
                    {
                        var line = port1.ReadLine();
                        var sections = line.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                        if (sections.Count() >= 2)
                        {
                            var elements = sections[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            List<double> elements2 = new List<double>();
                            foreach (var element in elements)
                            {
                                double.TryParse(element, out double temp1);
                                elements2.Add(temp1);
                            }

                            switch (sections[0])
                            {
                                case "accel":
                                    if (elements2.Count >= 4)
                                    {
                                        AccelRange = elements2[3];
                                        AccelX = (elements2[0] / 256.0) * (AccelRange / 2.0);
                                        AccelY = (elements2[1] / 256.0) * (AccelRange / 2.0);
                                        AccelZ = (elements2[2] / 256.0) * (AccelRange / 2.0);

                                        if (AccelCalibrateEn)
                                        {
                                            if (AccelX < AccelXMin)
                                            {
                                                AccelXMin = AccelX;
                                            }
                                            else if (AccelX > AccelXMax)
                                            {
                                                AccelXMax = AccelX;
                                            }

                                            if (AccelY < AccelYMin)
                                            {
                                                AccelYMin = AccelY;
                                            }
                                            else if (AccelY > AccelYMax)
                                            {
                                                AccelYMax = AccelY;
                                            }

                                            if (AccelZ < AccelZMin)
                                            {
                                                AccelZMin = AccelZ;
                                            }
                                            else if (AccelZ > AccelZMax)
                                            {
                                                AccelZMax = AccelZ;
                                            }
                                        }
                                    }
                                    break;
                                case "angular velocity":
                                    if (elements2.Count >= 3)
                                    {
                                        GyroScale = elements2[3];
                                        AngVelX = (elements2[0] / Math.Pow(2.0, 16)) * GyroScale;
                                        AngVelY = (elements2[1] / Math.Pow(2.0, 16)) * GyroScale;
                                        AngVelZ = (elements2[2] / Math.Pow(2.0, 16)) * GyroScale;
                                    }
                                    break;
                                case "Temperature":
                                    if (elements2.Count >= 1)
                                    {
                                        Temperature = elements2[0];
                                    }
                                    break;
                                case "Pressure":
                                    if (elements2.Count >= 1)
                                    {
                                        Pressure = elements2[0];
                                    }
                                    break;
                                case "Altitude":
                                    if (elements2.Count >= 1)
                                    {
                                        Altitude = elements2[0];
                                    }
                                    break;
                                case "mag":
                                    if (elements2.Count >= 3)
                                    {
                                        CompassGain = elements2[3];
                                        double lsb = 0.92;
                                        switch (CompassGain)
                                        {
                                            case 0: lsb = 0.73; break;
                                            case 1: lsb = 0.92; break;
                                            case 2: lsb = 1.22; break;
                                            case 3: lsb = 1.52; break;
                                            case 4: lsb = 2.27; break;
                                            case 5: lsb = 2.56; break;
                                            case 6: lsb = 3.03; break;
                                            case 7: lsb = 4.35; break;
                                        }
                                        MagX = elements2[0] * lsb;
                                        MagY = elements2[1] * lsb;
                                        MagZ = elements2[2] * lsb;
                                    }
                                    break;
                                case "heading":
                                    if (elements2.Count >= 1)
                                    {
                                        Heading = elements2[0];
                                    }
                                    break;
                                default: break;
                            }
                        }
                    }
                }
            });
        }

        TaskFactory f = new TaskFactory();

        public double AccelX { get; set; }
        public double AccelY { get; set; }
        public double AccelZ { get; set; }
        public double AccelRange { get; set; }
        public bool AccelCalibrateEn { get; set; } = false;

        public double AccelXMin { get; set; } = 0;
        public double AccelXMax { get; set; } = 0;
        public double AccelYMin { get; set; } = 0;
        public double AccelYMax { get; set; } = 0;
        public double AccelZMin { get; set; } = 0;
        public double AccelZMax { get; set; } = 0;

        public double AngVelX { get; set; }
        public double AngVelY { get; set; }
        public double AngVelZ { get; set; }
        public double GyroScale { get; set; }

        public double Temperature { get; set; }
        public double Pressure { get; set; }
        public double Altitude { get; set; }

        public double MagX { get; private set; }
        public double MagY { get; private set; }
        public double MagZ { get; private set; }
        public double CompassGain { get; set; }
        public double Heading { get; private set; }
    }
}
